import React from 'react';
import Header from './components/common/Header/Header';
import { Route, Switch } from "react-router-dom";
import Home from "./components/Home/Home";
import About from "./components/About/About";
import Product from "./components/Product/Product";
import Details from "./components/Details/Details";
import Cart from "./components/Cart/Cart";
import ProductList from "./components/ProductList/ProductsList";
import PageError from './components/PageError/pageError';
import './App.css';

function App() {
  return (
    <React.Fragment>
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/productList" component={ProductList} />
        <Route exact path="/product" component={Product} />
        <Route exact path="/about" component={About} />
        <Route exact path="/details" component={Details} />
        <Route exact path="/cart" component={Cart} />
        <Route component={PageError} />
      </Switch>
    </React.Fragment>
  );
}

export default App;
