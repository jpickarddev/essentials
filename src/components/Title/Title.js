import React from "react"

const Title = (props) => {
    return (
        <div className="row">
            <div className="col-10 mx-auto m-2 text-center text-title">
                <h1>{props.name}</h1>
                <h2>{props.title}</h2>
            </div>

        </div>
    );

}
export default Title;