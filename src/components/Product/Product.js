import React from 'react';
import { Link } from 'react-router-dom';
import { ProductConsumer } from "../../Context";
import styled from "styled-components";

const Product = (props) => {
    const { id, title, price, img, inCart } = props.product; // create variables for all product properties
    const ProductWrapper = styled.div`
    .card{
        border-color:transparent;
        transition:all ls linear;
        padding: 1px;
    }
    .card-footer{
        background:transparent;
        border-top:transparent;
        transition:all ls linear;
    }
    &:hover{
        .card{
            border:0.04rem solid silver;
            box-shadow:2px 2px 5px 0px silver;
            border-radius: 5px 5px;
        }
        .card-footer{
            background: rgba(247, 247, 247);
            border-radius: 5px 5px;
        }
    }
    .img-container{
        position:relative;
        overflow:hidden;
    }
    .card-img-top{
        transition:all ls linear;
    }
    .img-container:hover .card-img-top{
        transform:scale(1.2);
    }
    .cart-btn{
        position absolute;
        bottom:0;
        right:0;
        padding:0.2rem 0.4rem;
        background: White;
        border:none;
        color:White;
        font-size:1.4rem;
        border-radius: 0.5rem 0 0 0;
        transform:translate(100%, 100%);
        transition:all ls linear;
    }
    .img-container:hover .cart-btn{
        transform: translate(0,0);
    }
    .cart-btn:hover {
        background-color: rgb(52,76,169);
        color: White;
        cursor:pointer;
    }
    `

    return (
        <ProductWrapper className="col-9 mx-auto col-md-6 col-lg-3 my-3">
            <div className="card">
                <div className="img-container p-5" onClick={() => console.log('You Clicked')}>
                    <Link to="./details">
                        <img src={img} alt="product" className="card-img-top" />
                    </Link>
                    <button className="cart-btn" disabled={inCart ? true : false} onClick={() => { console.log('added to cart') }}>
                        {inCart ? (<p className="text-capitalize mb-0" disabled>
                            {" "}
                            In Cart
                        </p>) : (
                                <i className="fas fa-cart-plus"></i>
                            )}
                    </button>
                </div>
                {/* card footer */}
                <div className="card-footer d-flex justify-content-between">
                    <p className="align-self-center mb-0">
                        {title}
                    </p>
                    <h5 className="text-blue font-italic mb-0">
                        <span className="mr-1">£</span>
                        {price}
                    </h5>
                </div>
            </div>
        </ProductWrapper>
    );
}

export default Product;