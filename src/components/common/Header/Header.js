import { Navbar, Nav } from 'react-bootstrap';
import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import "./header.css";

const Header = () => {
    return (
        <Navbar expand="lg">
            <Navbar.Brand href="/">
                <img
                    src="../images/logo.jpeg"
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt=""
                />
                <span className="navBarBrandText">Essentials</span>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <NavLink to="/" className="menuLink" exact activeClassName="activeStyle">Home</NavLink>
                    <NavLink to="/ProductList" className="menuLink" exact activeClassName="activeStyle">Product List</NavLink>
                    <NavLink to="/about" className="menuLink" exact activeClassName="activeStyle">About</NavLink>
                </Nav>
                <Link to="/cart" className="ml-auto">
                    <button className="cartBtn">
                        <i className="fas fa-cart-plus" /> My Cart
                        </button>
                </Link>
            </Navbar.Collapse>
        </Navbar>
    )
}
export default Header;