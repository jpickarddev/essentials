import React from 'react';
import Title from '../Title/Title';
import { ProductConsumer } from "../../Context";
import Product from "../Product/Product";

const ProductList = () => {
    return (
        <React.Fragment>
            <div className="py-5">
                <div className="container">
                    <Title name="Essential" title="Products" />
                    <div className="row">
                        <ProductConsumer>
                            {value => {
                                return value.products.map(product => {
                                    return <Product key={product.id} product={product} />;
                                });
                            }}
                        </ProductConsumer>
                    </div>
                </div>
            </div>
        </React.Fragment>

    );
}
export default ProductList;
