import React, { Component } from "react";
import { storeProducts, detailProduct } from './Data/data';

const ProductConetext = React.createContext();

//Provider
class ProductProvider extends Component {
    state = {
        products: storeProducts,
        detailProduct: detailProduct
    }

    handleDetail = () => {
        console.log("Hello from details");
    }

    addToCart = () => {
        console.log('Hello from add to cart');
    }

    render() {
        return (
            <ProductConetext.Provider
                value={{
                    ...this.state,  // using spread operator here to pull apart the array
                    handleDetail: this.handleDetail,
                    addToCart: this.addToCart
                }}>
                {this.props.children}
            </ProductConetext.Provider>
        )

    }
}

//Consumer
const ProductConsumer = ProductConetext.Consumer;

export { ProductProvider, ProductConsumer };